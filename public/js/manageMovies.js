function addMovie() {

    var form_data = new FormData();
    form_data.append("id", $("#id").val());
    form_data.append("title", $("#title").val());
    form_data.append("slug", $("#slug").val());
    form_data.append("poster_url", $("#poster_url").val());
    form_data.append("backdrop_url", $("#backdrop_url").val());
    form_data.append("plot", $("#plot").val());
    $.ajax({
        url: "/movie/add",
        type: "POST",
        processData: false, // important
        contentType: false, // important
        data: form_data ,
        success:
            function(result)
            {
                new PNotify({
                    title: 'Added',
                    text: result,
                    type: 'success'
                });
            },
        error:
            function (result) {
                new PNotify({
                    title: 'Validation Failure',
                    text:   result.responseJSON,
                    type: 'error'
                });
            }
    });
}

function removeMovie(id) {

    $.ajax({
        method: "delete",
        url: "/movie/remove",
        datatype: "json",
        data: { id: id},
        success: function(result) {
            new PNotify({
                title: 'Delete',
                text: result,
                type: 'success'
            });
            $("#"+id).addClass("hide");
        },
        error: function(result) {
            new PNotify({
                title: 'Delete',
                text: result.responseJSON,
                type: 'error'
            });
        }
    });
}

function editMovie(id) {

    var form_data = new FormData();
    form_data.append("id", id);
    form_data.append("title", $("#title" + "-" + id).val());
    form_data.append("slug", $("#slug" + "-" + id).val());
    form_data.append("poster_url", $("#poster_url" + "-" + id).val());
    form_data.append("backdrop_url", $("#backdrop_url" + "-" + id).val());
    form_data.append("plot", $("#plot" + "-" + id).val());
    form_data.append("release_date", $("#release_date" + "-" + id).val());
    $.ajax({
        url: "/movie/edit",
        type: "post",
        processData: false, // important
        contentType: false, // important
        data: form_data ,
        success:
            function(result)
            {
                new PNotify({
                    title: 'Updated',
                    text: result,
                    type: 'success'
                });
            },
        error:
            function (result) {
                new PNotify({
                    title: 'Validation Failure',
                    text:   result.responseJSON,
                    type: 'error'
                });
            }
    });
}

