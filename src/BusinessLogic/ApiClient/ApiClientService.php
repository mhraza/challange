<?php
/**
 * Created by PhpStorm.
 * User: naeem
 * Date: 07.05.19
 * Time: 10:07
 */

namespace App\BusinessLogic\ApiClient;

use App\Repository\MoviesRepository;
use Psr\Container\ContainerInterface;

class ApiClientService
{
    private $service_full;
    /**
     * @var MoviesRepository
     */
    private $moviesRepository;
    private $container;

    /**
     * ApiClientService constructor.
     * @param MoviesRepository $repository
     */
    public function __construct(MoviesRepository $repository, ContainerInterface $container)
    {
        $this->moviesRepository = $repository;
        $this->container = $container;
    }

    public function importMovies()
    {
    try {
    $service_url = $this->container->getParameter('server_url');
    $movies = json_decode(file_get_contents($service_url), true);
    foreach ($movies as $movie) {
        $movieDetails = $this->moviesRepository->findOneBy(array('movie_id' => $movie['_id']));
        if (!$movieDetails) {
            $this->moviesRepository->insertMovies($movie);
        }
    }

} catch (\Exception $ex) {
    var_dump($ex->getMessage());
    exit;
}

    }
}