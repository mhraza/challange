<?php
/**
 * Created by PhpStorm.
 * User: naeem
 * Date: 07.05.19
 * Time: 10:05
 */

namespace App\BusinessLogic\Movies;

use App\Repository\MoviesRepository;
use App\BusinessLogic\ApiClient\ApiClientService;
use Symfony\Component\HttpFoundation\Request;

class MoviesService
{
    /**
     * @var MoviesRepository
     */
    private $moviesRepository;

    /**
     * @var ApiClientService
     */
    private $apiClient;

    /**
     * MoviesService constructor.
     * @param MoviesRepository $repository
     */
    public function __construct(MoviesRepository $repository, ApiClientService $apiClient)
    {
        $this->moviesRepository = $repository;
        $this->apiClient = $apiClient;
    }

    /**
     * @return array
     */
    public function ImportMovies()
    {
        $this->apiClient->importMovies();

        return array(
            'status' => 200,
            'data' => "Movies synced completely"
        );

    }

    /**
     * @return \App\Entity\Movies[]
     */
    public function listMovies()
    {
        return $this->moviesRepository->findAll();

    }

    /**
     * @param $id
     * @return \App\Entity\Movies|null
     */
    public function getMovieById($id)
    {
        return $this->moviesRepository->find($id);

    }

    /**
     * @param Request $request
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addMovie(Request $request)
    {
        if (empty($request->get('id'))) {

            return array(
                'data' => 'id is required',
                'status' => 403
            );
        }
        $movieDetails = $this->moviesRepository->findOneBy(array('movie_id' => $request->get('id')));
        if (isset($movieDetails)) {

            return array(
                'data' => 'ID must be unique, movie with this ID already exist',
                'status' => 403
            );
        }
        if (empty($request->get('title'))) {

            return array(
                'data' => 'title is required',
                'status' => 403
            );
        }
        if (empty($request->get('slug'))) {

            return array(
                'data' => 'slug is required',
                'status' => 403
            );
        }
        if (empty($request->get('poster_url'))) {

            return array(
                'data' => 'poster_url is required',
                'status' => 403
            );
        }
        if (empty($request->get('backdrop_url'))) {

            return array(
                'data' => 'backdrop_url is required',
                'status' => 403
            );
        }
        if (empty($request->get('plot'))) {

            return array(
                'data' => 'plot is required',
                'status' => 403
            );
        }
        $params = array(
            '_id' => $request->get('id'),
            'slug' => $request->get('slug'),
            'title' => $request->get('title'),
            'posterURL' => $request->get('poster_url'),
            'backdropURL' => $request->get('backdrop_url'),
            'plot' => $request->get('plot'),
            'releaseDate' => Date("Y-m-d")
        );
        $this->moviesRepository->insertMovies($params);

        return array(
            'data' => 'movie added',
            'status' => 201
        );


    }

    /**
     * @param Request $request
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeMovie(Request $request)
    {
        if (empty($request->get('id'))){

            return array(
                'data' => 'movie id must be provided',
                'status' => 403
            );
        }
        $movie = $this->getMovieById($request->get('id'));
        if(!isset($movie)) {

            return array(
                'data' => 'movie does not exist',
                'status' => 403
            );
        }
        $this->moviesRepository->removeMovies($movie);

        return array(
            'data' => 'movie removed successfully',
            'status' => 202
        );
    }

    /**
     * @param Request $request
     * @return array
     */
    public function editMovie(Request $request)
    {
        if (empty($request->get('id'))) {

            return array(
                'data' => 'id is required',
                'status' => 403
            );
        }
        if (empty($request->get('title'))) {

            return array(
                'data' => 'title is required',
                'status' => 403
            );
        }
        if (empty($request->get('slug'))) {

            return array(
                'data' => 'slug is required',
                'status' => 403
            );
        }
        if (empty($request->get('poster_url'))) {

            return array(
                'data' => 'poster_url is required',
                'status' => 403
            );
        }
        if (empty($request->get('backdrop_url'))) {

            return array(
                'data' => 'backdrop_url is required',
                'status' => 403
            );
        }
        if (empty($request->get('plot'))) {

            return array(
                'data' => 'plot is required',
                'status' => 403
            );
        }
        if (empty($request->get('release_date'))) {

            return array(
                'data' => 'release_date is required',
                'status' => 403
            );
        }
        $params = array(
            'id' => $request->get('id'),
            'slug' => $request->get('slug'),
            'title' => $request->get('title'),
            'posterURL' => $request->get('poster_url'),
            'backdropURL' => $request->get('backdrop_url'),
            'plot' => $request->get('plot'),
            'releaseDate' => $request->get('release_date')
        );
        $this->moviesRepository->editMovies($params);

        return array(
            'data' => 'movie updated',
            'status' => 201
        );
    }

    public function getPaginatedMovies(int $from, int $to)
    {
        return $this->moviesRepository->getMovies($from, $to);
    }


}