<?php

namespace App\Repository;

use App\Entity\Movies;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Psr\Container\ContainerInterface;

/**
 * @method Movies|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movies|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movies[]    findAll()
 * @method Movies[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MoviesRepository extends ServiceEntityRepository
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * MoviesRepository constructor.
     * @param RegistryInterface $registry
     * @param ContainerInterface $container
     */
    public function __construct(RegistryInterface $registry, ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct($registry, Movies::class);
    }

    /**
     * @param array $params
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function insertMovies(array $params)
    {
        $movie = new Movies();
        $movie->setMovieId($params['_id']);
        $movie->setTitle($params['title']);
        $movie->setReleaseDate(new \DateTime($params['releaseDate']));
        $movie->setPosterUrl($params['posterURL']);
        $movie->setBackdropUrl($params['backdropURL']);
        $movie->setPlot($params['plot']);
        $movie->setSlug($params['slug']);
        $this->_em->persist($movie);
        $this->_em->flush($movie);

    }

    /**
     * @param Movies $movie
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeMovies(Movies $movie)
    {
        $this->_em->remove($movie);
        $this->_em->flush();
    }

    public function editMovies(array $params)
    {
        $movie = $this->find($params['id']);
        $movie->setTitle($params['title']);
        $movie->setReleaseDate(new \DateTime($params['releaseDate']));
        $movie->setPosterUrl($params['posterURL']);
        $movie->setBackdropUrl($params['backdropURL']);
        $movie->setPlot($params['plot']);
        $movie->setSlug($params['slug']);
        $this->_em->persist($movie);
        $this->_em->flush($movie);

    }

    public function getMovies(int $from, int $to)
    {
        $query = $this->_em->createQueryBuilder()
            ->addSelect('movies')
            ->from('App\Entity\Movies', 'movies')
            ->getQuery();
        $paginator  = $this->container->get('knp_paginator');
        $profiles = $paginator->paginate($query, $from, $to
        );


        return $profiles;

    }
}
